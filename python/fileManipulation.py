#!/usr/bin/python

from pdb import set_trace as ST
import re
import ast
from subprocess import call, check_output
import os

class fileManipulation:
    def __init__(self, inputFilename, outputFilename,commentStyle='% ',pythonStart='% python start',pythonEnd='% python end'):
        # define the filenames
        self.inputFilename = inputFilename
        self.outputFilename = outputFilename

        # define the comment style, the python start and end identifiers
        self.commentStyle = commentStyle
        self.pythonStart = pythonStart
        self.pythonEnd = pythonEnd

    def updateEvaluation(self, test=0, figuresFolder='figures/') :
        # get the input file
        f_in = open(self.inputFilename,'r')
        f_in_str = f_in.read()
        f_in.close()

        # start writing the output
        f_out_str = ''

        # find the first occurence of the python identifier
        start_re = re.search(self.pythonStart,f_in_str)
        while start_re :
            start = start_re.start()
            # get the dictionary that dirigates the updated version of the code
            startDict = re.search('\n'+self.commentStyle,f_in_str[start:]).end()
            endDict = re.search('\n',f_in_str[start+startDict:]).start()
            currentDict = ast.literal_eval(f_in_str[start+startDict:start+startDict+endDict])
            # update the code and put it into the output string
            updatedCode = self.evaluation(currentDict,test,figuresFolder)
            end = re.search(self.pythonEnd,f_in_str[start:]).start()
            if updatedCode :
                f_out_str += f_in_str[:start+startDict+endDict]
                f_out_str += '\n'+updatedCode+'\n'
            else :
                f_out_str +=f_in_str[:start+end]
            f_in_str = f_in_str[start+end:]
            start_re = re.search(self.pythonStart,f_in_str)

        # put the string inside an output file
        f_out_str += f_in_str
        f_out = open(self.outputFilename,'w')
        f_out.write(f_out_str)
        f_out.close()

    def evaluation(self,evaluationInfo,test=0, figuresFolder='figures/') :
        """adapt"""
        print evaluationInfo
        curFolder = check_output(['pwd'])[:-1]
        os.chdir(evaluationInfo["evaluationsFolder"])
        # ls to list of the files inside
        # run over all the files and fill a dictionary with the questions and a list of the responses

        filesInEvaluationDirectory = check_output(['ls'])
        evaluationFiles = filesInEvaluationDirectory.split('\n')[:-1]
        templateSection = evaluationInfo["templateSection"]
        templateQuestion = evaluationInfo["templateQuestion"]
        templateQuestionWithTextAnswer = evaluationInfo["templateQuestionWithTextAnswer"]
        templateTitle = evaluationInfo["templateTitle"]

        answersOfAllEvaluations = []
        for curEvaluationFile in evaluationFiles :
            sections,titles,questions,bestResponses,worstResponses,possibleAnswers,answers,multipleAnswers = self.getResponsesToQuestions(curEvaluationFile)
            # print sections
            # print titles
            # print questions
            # print bestResponses
            # print worstResponses
            # print possibleAnswers
            # print answers
            # ST()
            for questionId in range(len(questions)) :
                if len(answersOfAllEvaluations) <= questionId :
                    answersOfAllEvaluations.append([])
                # print answers[questionId]
                # if bestResponses[questionId] :
                if test and possibleAnswers[questionId]:
                    import random
                    for i in range(test) :
                        randomAnswerId = random.randint(0,len(possibleAnswers[questionId])-1)
                        answersOfAllEvaluations[questionId].append((possibleAnswers[questionId])[randomAnswerId])
                    # print possibleAnswers[questionId]
                    # print answersOfAllEvaluations[questionId]
                    # ST()
                else :
                    answersOfAllEvaluations[questionId].append(answers[questionId])
                    

                # updatedEvaluation += templateQuestion.replace('QUESTION', curQuestion)
        os.chdir(curFolder)

        call(['mkdir',figuresFolder])

        placeToStoreFigures = figuresFolder+"question_"
        import matplotlib
        matplotlib.use('PDF')
        
        for questionId in range(len(questions)) :
            if bestResponses[questionId] :
                answerOccurences = [0]*5
                average = 0
                for i in range(len(possibleAnswers[questionId])) :
                    answerOccurences[i] = answersOfAllEvaluations[questionId].count((possibleAnswers[questionId])[i])
                    average += float(answerOccurences[i])*(i+1)
                numberOfAllOccurences = sum(answerOccurences)
                # print answerOccurences
                # print answersOfAllEvaluations[questionId]
                # ST()
                average /= numberOfAllOccurences
                # print average
                standardDeviation = 0
                for i in range(5) :
                    standardDeviation += (answerOccurences[i]-average)**2
                    answerOccurences[i] *= 100/numberOfAllOccurences
                import math
                standardDeviation /= numberOfAllOccurences
                standardDeviation = math.sqrt(standardDeviation)
                import matplotlib.pyplot as plt
                # plt.plot(range(1,6), answerOccurences)
                width = 1/1.5
                plt.figure(figsize=(10,2))
                plt.bar(range(1,6), answerOccurences, width, color="blue", align='center')
                plt.plot((average,average), (0,100),'r',linewidth=2.0)
                # plt.plot((average-standardDeviation,average+standardDeviation), (90,90),'r',linewidth=2.0)
                plt.xlim([0.5,5.5])
                plt.ylim([0,100])
                plt.text(0.25, 90, bestResponses[questionId], ha='right',size=18)
                plt.text(5.75, 90, worstResponses[questionId],size=18)
                plt.savefig(placeToStoreFigures+str(questionId),bbox_inches='tight')
                plt.clf()
                plt.close()
                # plot here and save it ... figures_OOP/question--.
            elif not multipleAnswers[questionId] and possibleAnswers[questionId]: 
                chosenAnswers = list(set(answersOfAllEvaluations[questionId]))
                answerOccurences = [0]*len(possibleAnswers[questionId])
                for i in range(len(possibleAnswers[questionId])) :
                    answerOccurences[i] = answersOfAllEvaluations[questionId].count((possibleAnswers[questionId])[i])
                numberOfAllOccurences = sum(answerOccurences)
                for i in range(len(possibleAnswers[questionId])) :
                    answerOccurences[i] *= 100/numberOfAllOccurences
                # print answerOccurences
                from pylab import *
                import matplotlib
                pos = arange(len(possibleAnswers[questionId]))+.1
                fig = plt.figure(figsize=(10,0.6*len(possibleAnswers[questionId])+1))
                ax = fig.add_subplot(1, 1, 1)
                barh(pos,answerOccurences, align='center')
                yticks(pos, possibleAnswers[questionId])

                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                xlim([0,100])
                plt.savefig(placeToStoreFigures+str(questionId),bbox_inches='tight')
                plt.clf()
                plt.close()


            # return updatedEvaluation
        updatedEvaluation = ''
        for questionId in range(len(questions)) :
            if sections[questionId] :
                curSection = templateSection.replace('SECTION', sections[questionId])
                updatedEvaluation += curSection
            if titles[questionId] :
                curTitle = templateTitle.replace('TITLE', titles[questionId])
                updatedEvaluation += curTitle
            if not multipleAnswers[questionId] :
                curQuestionLatex = templateQuestion.replace('QUESTION', questions[questionId])
                curQuestionLatex = curQuestionLatex.replace('FIGUREFILENAME', placeToStoreFigures+str(questionId)+".pdf" )
                updatedEvaluation += curQuestionLatex
            else :
                curQuestionLatex = templateQuestionWithTextAnswer.replace('QUESTION', questions[questionId])
                # print (answersOfAllEvaluations[questionId])
                allAnswers = ''
                for i in range(len(answersOfAllEvaluations[questionId])) :
                    for j in range(len((answersOfAllEvaluations[questionId])[i])) :
                        allAnswers += '\n& '+((answersOfAllEvaluations[questionId])[i])[j]
                curQuestionLatex = curQuestionLatex.replace('ANSWERS', allAnswers)
                updatedEvaluation += curQuestionLatex
            # if not multipleAnswers[questionId] :
            #     curQuestionLatex = curQuestionLatex.replace('ANSWERS', ' '.join(answersOfAllEvaluations[questionId]))
        # print answersOfAllEvaluations

        # ST()

        return updatedEvaluation


    def getResponsesToQuestions(self,curEvaluationFile) :#curDictOfQuestionsAndResponses
        # get the input file
        call(['pwd'])
        print curEvaluationFile
        f_in = open(curEvaluationFile,'r')
        f_in_str = f_in.read()
        f_in.close()
        f_in_str

        # find the first occurence of the python identifier
        questionIdentifier = '\n(1\-|[a-zA-Z0-9]*,|Separate every new answer)'
        startQuestion_re = re.search('\n.*'+questionIdentifier,f_in_str)

        sections = []
        titles = []
        questions = []
        bestResponses = []
        worstResponses = []
        answers = []
        multipleAnswers = []
        possibleAnswers = []
        while startQuestion_re :
            # get the next section
            startSection_re = re.search('[0-9]+\..*',f_in_str[:startQuestion_re.start()+1])
            if startSection_re :
                startSection = startSection_re.start()
                endSection = re.search('[0-9]+\..*\n',f_in_str[:startQuestion_re.start()+1]).end()
                sections.append(f_in_str[startSection+3:endSection-1])
            else :
                sections.append(None)
            # get the next title
            startTitle_re = re.search('Please.*',f_in_str[:startQuestion_re.start()+1])
            if startTitle_re :
                startTitle = startTitle_re.start()
                endTitle = re.search('Please.*\n',f_in_str[:startQuestion_re.start()+1]).end()
                titles.append(f_in_str[startTitle:endTitle-1])
            else :
                titles.append(None)

            # get the next question
            startQuestion = startQuestion_re.start()+1
            endQuestion_re = re.search(questionIdentifier,f_in_str)
            endQuestion = endQuestion_re.start()
            curQuestion = f_in_str[startQuestion:endQuestion]
            questions.append(curQuestion)
            f_in_str = f_in_str[endQuestion+1:]

            # get the best and the worst response (if it applies)
            if f_in_str[:2] == '1-':
                endBestResponse_re = re.search(', \.\.\.',f_in_str)
                endBestResponse = endBestResponse_re.start()
                curBestResponse = f_in_str[2:endBestResponse]
                bestResponses.append(curBestResponse)
                startWorseResponse_re = re.search('5\-',f_in_str)
                startWorseResponse = startWorseResponse_re.start()+2
                endWorseResponse_re = re.search('\n',f_in_str)
                endWorseResponse = endWorseResponse_re.start()
                curWorstResponse = f_in_str[startWorseResponse:endWorseResponse]
                worstResponses.append(curWorstResponse)
                possibleAnswers.append(['1','2','3','4','5'])
                f_in_str = f_in_str[endWorseResponse+1:]
                multipleAnswers.append(0)
            elif f_in_str[:25] == 'Separate every new answer':
                bestResponses.append(None)
                worstResponses.append(None)
                possibleAnswers.append(None)
                endLine = re.search('\n',f_in_str).end()
                f_in_str = f_in_str[endLine:]
                multipleAnswers.append(1)
            else :
                bestResponses.append(None)
                worstResponses.append(None)
                endLine = re.search('\n',f_in_str).end()
                curPossibleAnswers = [f_in_str[:endLine-1]]
                while re.search(', ',curPossibleAnswers[len(curPossibleAnswers)-1]) :
                    # curPossibleAnswers.insert(0,curPossibleAnswers[len(curPossibleAnswers)-1].split(', ', 1 )[0])
                    newSplit = curPossibleAnswers[len(curPossibleAnswers)-1].split(', ', 1 )
                    curPossibleAnswers.insert(len(curPossibleAnswers)-1,newSplit[0])
                    curPossibleAnswers[len(curPossibleAnswers)-1] = newSplit[1]
                    # print curPossibleAnswers
                # ST()
                possibleAnswers.append(curPossibleAnswers)
                # possibleAnswers.append(f_in_str[:endLine-1].split(', '))
                # print possibleAnswers
                # ST()
                f_in_str = f_in_str[endLine:]
                multipleAnswers.append(0)

            # get the answer
            answerEnd_re = re.search('\n',f_in_str)
            answerEnd = answerEnd_re.start()
            curAnswer = f_in_str[0:answerEnd]
            # print curQuestion, curBestResponse, curWorstResponse, curAnswer
            curAnswers = []
            # print curAnswer
            if len(curAnswer) :
                while answerEnd and curAnswer[0] == "-":
                    # print curAnswer
                    curAnswers.append(curAnswer[2:])
                    f_in_str = f_in_str[answerEnd+1:]
                    answerEnd_re = re.search('\n',f_in_str)
                    answerEnd = answerEnd_re.start()
                    curAnswer = f_in_str[0:answerEnd]
            # print curAnswers
            if len(curAnswers) :
                answers.append(curAnswers)
            else :    
                answers.append(curAnswer)


            # answers.append(curAnswer)



            startQuestion_re = re.search('\n.*'+questionIdentifier,f_in_str)

        # print answers
        # ST()

        # print "Number of questions:", len(questions)
        return sections,titles,questions,bestResponses,worstResponses,possibleAnswers,answers,multipleAnswers